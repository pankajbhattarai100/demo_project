from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.core.files import File
from demo_project.settings import BASE_DIR
from .models import DemoObject, DemoFile, DemoWebcamImage
import cv2, json, os, base64
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage


DATA_PATH = os.path.join(BASE_DIR, 'data/')
CAP_PATH = os.path.join(BASE_DIR, 'captured/')


def clean_dir(path):
    """
    A function to clean the specified directory.
    """
    success = True
    message = ""

    try:
        dir = path
        for f in os.listdir(dir):
            os.remove(os.path.join(dir, f))

    except Exception as e:
        success = False
        message = e

    datas = {
        "success": success,
        "message": message
    }
    return datas

def upload(request):
    """
    A function to upload the files and images to entered name.
    """
    if request.method == 'POST': 
        try:
            name = request.POST['name']
            image_data = request.POST.get('webcam_images')
            image_list = image_data.split('data:image/png')
            webcam_images = []

            folder_path = os.path.join(DATA_PATH, name)

            if not os.path.exists(folder_path):
                os.makedirs(folder_path)

            for item in image_list:
                if item == 'data:image/png' or item == '':
                    continue
                else:
                    webcam_images.append(item)
            

            for index, webcam_image in enumerate(webcam_images):
                imgstr = webcam_image.split('base64,')[1]
                data = ContentFile(base64.b64decode(imgstr))
                myfile = "{}.png".format(index+1)
                fs = FileSystemStorage(location=folder_path)
                filename = fs.save(myfile, data)

            for item in request.FILES.getlist('files'):
                file_name = os.path.join(folder_path, item.name)
                with open(file_name, '+wb') as f:
                    for chunk in item.chunks():
                        f.write(chunk)

            messages.success(request, 'Data added to folder {}'.format(name))
            
        except Exception as e:
            messages.error(request, e)

        clean_dir(CAP_PATH) #remove the duplicate data
        return redirect('upload')
    else:
        return render(request, 'demo_app/upload_form.html')


def capture_images(request):
    """
    A function to activate and capture images using webcam.
    """
    if not os.path.exists(CAP_PATH):
        os.makedirs(CAP_PATH)

    try:
        cam = cv2.VideoCapture(0)
        cv2.namedWindow("Capture")

        img_counter = 1
        img_list = []

        while True:
            ret, frame = cam.read()

            if not ret:
                break

            cv2.imshow("Capture", frame)
            k = cv2.waitKey(1)

            if k%256 == 27 or cv2.getWindowProperty("Capture", cv2.WND_PROP_VISIBLE) < 1:
                # ESC or X pressed
                break

            elif k%256 == 32:
                # SPACE pressed
                img_name = "image_{}.png".format(img_counter)
                cv2.imwrite(CAP_PATH + img_name, frame)
                
                img_list.append(img_name)
                img_counter += 1

        cam.release()
        cv2.destroyAllWindows()

        datas = {
            "success": True,
            "img_list": img_list,
            "count": len(img_list)
        }

    except Exception as e:
        datas ={
            "success": False,
            "message": e
        }

    return HttpResponse(json.dumps(datas), content_type='application/json')

def reset_captured_images(request):
    """
    A futnction to reset the captured images.
    """
    datas = clean_dir(CAP_PATH)
    return HttpResponse(json.dumps(datas), content_type='application/json')