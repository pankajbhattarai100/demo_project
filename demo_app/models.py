from django.db import models
import os


def get_upload_path(instace, filename):
    return os.path.join("{}".format(instace.demo_id.name), filename)


class DemoObject(models.Model):
    """
    A modal to store the demo objects.
    """
    class Meta:
        verbose_name_plural = "Demo Objects"

    name = models.CharField(max_length=266)

    def __str__(self):
        return self.name
    


class DemoFile(models.Model):
    """
    A modal to files related to DemoApp.
    """
    class Meta:
        verbose_name_plural = "Demo Files"

    demo_id = models.ForeignKey(DemoObject, verbose_name="Demo Object", on_delete=models.CASCADE)
    demo_file = models.FileField(upload_to=get_upload_path)

    def __str__(self):
        return os.path.basename(self.demo_file.name)
    


class DemoWebcamImage(models.Model):
    """
    A modal to store the images uploaded from webcam.
    """
    class Meta:
        verbose_name_plural = "Demo Webcam Images"

    demo_id = models.ForeignKey(DemoObject, verbose_name="Demo Object", on_delete=models.CASCADE)
    demo_image = models.ImageField(upload_to=get_upload_path)

    def __str__(self):
        return os.path.basename(self.demo_image.name)
    