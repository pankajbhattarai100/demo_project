from django.contrib import admin
from .models import DemoObject, DemoFile, DemoWebcamImage

admin.site.register(DemoObject)


@admin.register(DemoFile)
class DemoFileAdmin(admin.ModelAdmin):
    list_display = ['demo_id', 'demo_file']


@admin.register(DemoWebcamImage)
class DemoWebcamImageAdmin(admin.ModelAdmin):
    list_display = ['demo_id', 'demo_image']