(function() {
    var width = 435;
    var height = 0;

    var streaming = false;

    var video = null;
    var canvas = null;
    var photo = null;
    var btn_capture = null;
    var images = [];
    var tracks = null;
    var isNewCapture = true;

    function startup() {
        video = document.getElementById('video');
        canvas = document.getElementById('canvas');
        photo = document.getElementById('photo');

        btn_capture = document.getElementById('btn_webcam_capture');
        btn_close = document.getElementById('btn_webcam_close');

        navigator.mediaDevices.getUserMedia({
            video: true,
            audio: false
        })
        .then(function(stream) {
            video.srcObject = stream;
            video.play();
            tracks = stream.getTracks()
        })
        .catch(function(err) {
            console.log("An error occurred: " + err);
        });

        video.addEventListener('canplay', function(ev) {
            if (!streaming) {
                height = video.videoHeight / (video.videoWidth / width);

                if (isNaN(height)) {
                    height = width / (4 / 3);
                }

                video.setAttribute('width', width);
                video.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', height);
                streaming = true;
            }
            video.style.display = '';
            btn_capture.style.display = '';
            btn_close.style.display = '';
            btn_open.style.display = 'none';
        }, false);

        if(isNewCapture){
            isNewCapture = false;
            btn_capture.addEventListener('click', function(ev) {
                takepicture();
                ev.preventDefault();
            }, false);
        }
        
        btn_close.addEventListener('click', function() {
            btn_open.style.display = '';
            video.style.display = 'none';
            btn_capture.style.display = 'none';
            btn_close.style.display = 'none';
            tracks.forEach(function(track) {
                track.stop();
            });
        })
        clearphoto();
    }


    function clearphoto() {
        var context = canvas.getContext('2d');
        context.fillStyle = "#AAA";
        context.fillRect(0, 0, canvas.width, canvas.height);

        var data = canvas.toDataURL('image/png');
        photo.setAttribute('src', data);
    }

    function takepicture() {
        if(images.length <= 7){
            var context = canvas.getContext('2d');
            if (width && height) {
                canvas.width = width;
                canvas.height = height;
                context.drawImage(video, 0, 0, width, height);

                var data = canvas.toDataURL('image/png');
                photo.setAttribute('src', data);
                images.push(data);
                $('#images_count').text(images.length + ' images captured');
                $('#id_webcam_images').val(images)
            } else {
                clearphoto();
            }
        }
        else{
            alert('Cannot upload more than 8 photos at once.');
        }
        
    }
    btn_open = document.getElementById('btn_webcam_open');
    btn_open.onclick = function () {
        startup()
    }
    btn_reset = document.getElementById("btn_reset");
    btn_reset.onclick = function () {
        images = []
        $('#images_count').text('No images captured');
    }
})();