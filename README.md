This project is for the task provided by treeleaf.

# TO RUN THIS PROJECT

## Clone the repo
    git clone https://gitlab.com/pankajbhattarai100/demo_project.git

## Create a virtual env
    python3 -m venv <environment_name>

## Activate the Environmane
    source <environment_name>/bin/activate

## Install the Requirements
    pip install -r requirements.txt

## Make Migrations and Migrate
    python manage.py makemigrations demo_app && python manage.py migrate => this might need to be done explicitely

## Create Superuser
    python manage.py createsuperuser => provide necessary details

## Finally run the server using,
    python manage.py runserver => go to localhost:8000 to access the app